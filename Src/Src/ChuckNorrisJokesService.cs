using System.Threading.Tasks;
using BeeITGrpc;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace GrpcDemo.Server
{
    public class ChuckNorrisJokesService : ChuckNorrisJokes.ChuckNorrisJokesBase
    {
        private readonly ILogger<ChuckNorrisJokesService> _logger;
        private readonly ExternalApiClient _client;
        private readonly Repository _repository;

        public ChuckNorrisJokesService(ILogger<ChuckNorrisJokesService> logger, ExternalApiClient client, Repository repository)
        {
            _logger = logger;
            _client = client;
            _repository = repository;
        }

        public override async Task<NewChuckNorrisJoke> GetRandomJoke(EmptyMessage _, ServerCallContext __)
        {
            return await _client.GetRandomJoke();
        }

        public override async Task<EmptyMessage> AddJoke(NewChuckNorrisJoke joke, ServerCallContext _)
        {
            await _repository.Add(joke);
            return new EmptyMessage();
        }

        public override async Task<ChuckNorrisJokeListResponse> ListJokes(EmptyMessage joke, ServerCallContext _)
        {
            return await _repository.GetList();
        }

        public override async Task<EmptyMessage> DeleteJokeById(NewChuckNorrisJokePrimaryId jokeId, ServerCallContext _)
        {
            await _repository.DeleteById(jokeId.Id);
            return new EmptyMessage();
        }
    }
}
