using Microsoft.Data.Sqlite;

namespace GrpcDemo.Server
{
    public class ConnectionFactory
    {
        private readonly string _connectionString;

        public ConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqliteConnection CreateConnection() => new(_connectionString);
    }
}