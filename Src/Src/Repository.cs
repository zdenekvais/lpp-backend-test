using System.Threading.Tasks;
using BeeITGrpc;
using Microsoft.Data.Sqlite;

namespace GrpcDemo.Server
{
    public class Repository
    {
        private readonly ConnectionFactory _connectionFactory;

        public Repository(ConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        private static ChuckNorrisJoke jokeDtoFromReader(SqliteDataReader reader)
        {
            return new ChuckNorrisJoke
            {
                PrimaryId = reader["primaryId"].ToString(),
                Id = reader["id"].ToString(),
                Value = reader["value"].ToString(),
                Url = reader["url"].ToString(),
                IconUrl = reader["iconUrl"].ToString(),
            };
        }

        public async Task<ChuckNorrisJokeListResponse> GetList()
        {
            var connection = _connectionFactory.CreateConnection();
            await connection.OpenAsync();
            var jokes = new ChuckNorrisJokeListResponse();

            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM jokes ORDER BY primaryId DESC";
            using var reader = await command.ExecuteReaderAsync();

            while (reader.Read())
            {
                jokes.Jokes.Add(jokeDtoFromReader(reader));
            }
            await connection.CloseAsync();

            return jokes;
        }

        public async Task Add(NewChuckNorrisJoke joke)
        {
            await using var connection = _connectionFactory.CreateConnection();
            await connection.OpenAsync();

            var command = connection.CreateCommand();
            command.CommandText = "INSERT INTO jokes (id, url, value, iconUrl) VALUES ($id, $url, $value, $iconUrl)";
            command.Parameters.AddWithValue("$id", joke.Id);
            command.Parameters.AddWithValue("$url", joke.Url);
            command.Parameters.AddWithValue("$value", joke.Value);
            command.Parameters.AddWithValue("$iconUrl", joke.IconUrl);
            await command.ExecuteNonQueryAsync();

            await connection.CloseAsync();
        }

        public async Task DeleteById(string id)
        {
            var connection = _connectionFactory.CreateConnection();
            await connection.OpenAsync();

            var command = connection.CreateCommand();
            command.CommandText = "DELETE FROM jokes WHERE primaryId = $primaryId";
            command.Parameters.AddWithValue("$primaryId", id);
            await command.ExecuteNonQueryAsync();

            await connection.CloseAsync();
        }
    }
}